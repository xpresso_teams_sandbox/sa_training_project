"""Module for data understand and exploration"""

__all__ = ['Explorer']
__author__ = 'Sanyog Vyawahare'

import pandas as pd

from xpresso.ai.core.commons.exceptions.xpr_exceptions import InputLengthMismatch
from xpresso.ai.core.commons.utils.constants import \
    EXPLORE_UNIVARIATE_FILENAME, \
    EXPLORER_OUTPUT_PATH, DEFAULT_PROBABILITY_BINS, EXPLORE_MULTIVARIATE_FILENAME
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.exploration.data_type import DataType
from xpresso.ai.core.data.exploration.render_exploration import RenderExploration
from xpresso.ai.core.data.visualization.abstract_visualization import PlotType
from xpresso.ai.core.logging.xpr_log import XprLogger

# This is indented as logger can not be serialized and can not be part
# of automl
logger = XprLogger()


class Explorer:
    """Understands and Explore the data"""

    def __init__(self, dataset):
        self.dataset = dataset

    def understand(self, verbose=True):
        """Understands and assigns the datatype to the attributes
        Args:
            verbose('bool'): If True then renders the output"""
        if not isinstance(self.dataset.type, DatasetType):
            logger.warning(
                "Unsupported datatype provided to understand")
            return

        self.dataset.info.understand_attributes(self.dataset.data, self.dataset.type)
        for attr in self.dataset.info.attributeInfo:
            if attr.type == DataType.NUMERIC.value:
                self.dataset.data[attr.name] = self.dataset.data[attr.name].astype(
                    DataType.FLOAT.value)

        RenderExploration(self.dataset).render_understand(verbose=verbose)

    def explore_univariate(self, verbose=True, to_excel=False,
                           validity_threshold=None,
                           output_path=EXPLORER_OUTPUT_PATH,
                           file_name=EXPLORE_UNIVARIATE_FILENAME,
                           bins=DEFAULT_PROBABILITY_BINS):
        """
        Univariate analysis of all the attributes
        Args:
            verbose('bool'): If True then renders the output
            to_excel('bool'): If True saves the output to excel file
            output_path(str): path where the multivariate excel file to be
                stored
            file_name(str): file name of excel file
            validity_threshold(int): percent value for garbage threshold
            bins(int): No. of buckets for bar graph, default 20
        """

        if self.dataset.type is not DatasetType.DIST_STRUCTURED or not isinstance(
                self.dataset.type, DatasetType):
            logger.warning(
                "Unsupported datatype provided to explore_univariate")
            return
        self.dataset.info.populate_attribute(self.dataset.data,
                                             self.dataset.type,
                                             validity_threshold,
                                             bins=bins)

        RenderExploration(self.dataset).render_univariate(
            verbose=verbose, to_excel=to_excel, output_path=output_path,
            file_name=file_name)

    def explore_multivariate(self, verbose=True, to_excel=False,
                             output_path=EXPLORER_OUTPUT_PATH,
                             file_name=EXPLORE_MULTIVARIATE_FILENAME):
        """
        Multivariate analysis of all the attributes
        Args:
            verbose('bool'): If True then renders the output
            to_excel('bool'): If True saves the output to excel file
            output_path(str): path where the multivariate excel file to be
                stored
            file_name(str): file name of excel file
        """

        if self.dataset.type != DatasetType.STRUCTURED or not isinstance(
                self.dataset.type, DatasetType):
            logger.warning(
                "Unsupported datatype provided to explore_multivariate")
            return

        self.dataset.info.populate_metric(self.dataset.data, self.dataset.type)

        RenderExploration(self.dataset).render_multivariate(
            verbose=verbose, to_excel=to_excel, output_path=output_path,
            file_name=file_name)

    def explore_target(self, target):
        """
        Target attribute analysis against other attributes in the dataset
        Args:
            target('str'): Target attribute from given dataset
        """

        attr_list = list(filter(lambda attr: attr.name == target,
                                self.dataset.info.attribute_info))
        if not attr_list:
            logger.error("{} attribute doesn't exist".format(target))
            return
        target_attr = attr_list[0]

        for attr in self.dataset.info.attribute_info:
            if target_attr.type in [DataType.NOMINAL.value,
                                    DataType.ORDINAL.value] \
                    and attr.type == DataType.NUMERIC.value:
                self.get_target_metrics(attr, target_attr, PlotType.QUARTILE.value)
                self.get_target_metrics(attr, target_attr, PlotType.DENSITY.value)
            elif target_attr.type in [DataType.NOMINAL.value,
                                      DataType.ORDINAL.value] \
                    and attr.type in [DataType.NOMINAL.value,
                                      DataType.ORDINAL.value]:
                self.get_target_metrics(attr, target_attr, PlotType.BAR.value)
                self.get_target_metrics(attr, target_attr, PlotType.MOSAIC.value)
            elif target_attr.type == DataType.NUMERIC.value \
                    and attr.type == DataType.NUMERIC.value:
                self.get_target_metrics(attr, target_attr, PlotType.SCATTER.value)
            elif target_attr.type == DataType.NUMERIC.value \
                    and attr.type in [DataType.NOMINAL.value,
                                      DataType.ORDINAL.value]:
                self.plot_target(attr, target_attr, PlotType.QUARTILE.value)

    def get_target_metrics(self, attr, target_attr, plot_type):
        """
            Calculate metrics for Target analysis.
            Args:
                attr(:obj attribute_info): object containing metrics for
                    specific attribute in dataset
                target_attr(:obj attribute_info): object containing metrics
                    for target attribute
                plot_type(:str): Specifies the type of plot
        """

        attr_data = self.dataset.data[attr.name]
        target_data = self.dataset.data[target_attr.name]

        if len(attr_data) != len(target_data):
            raise InputLengthMismatch()
        data = pd.DataFrame(list(zip(attr_data, target_data)), columns=[0, 1])
        if plot_type == PlotType.BAR.value:
            new_data = pd.DataFrame(data[0].value_counts())
            new_data.reset_index(inplace=True)
            new_data.rename(columns={"index": 'category_1', 0: "total"},
                            inplace=True)
            data = pd.DataFrame(data.groupby([0, 1]).size(), columns=["count"])
            data.reset_index(inplace=True)
            data.rename(columns={0: 'category_1', 1: 'category_2'}, inplace=True)
            bar_plot = {
                "data_count_per_category": new_data.to_dict(orient='list'),
                "freq_count": list(data.transpose().to_dict(orient='list')
                                   .values())
            }
            attr.metrics["bar_plot"] = bar_plot
        elif plot_type == PlotType.SCATTER.value:
            scatter_plot = {
                "data": data.to_dict(orient="list")
            }
            attr.metrics["scatter_plot"] = scatter_plot
        elif plot_type == PlotType.QUARTILE.value:
            box_plot = {
                "data": data.to_dict(orient="list")
            }
            attr.metrics["box_plot"] = box_plot
        elif plot_type == PlotType.DENSITY.value:
            data.rename(columns={0: "Input", 1: "Target"},
                        inplace=True)
            category_list = data.Target.unique()
            data_frame_dict = {category: pd.DataFrame for category in
                               category_list}
            for key in data_frame_dict.keys():
                data_frame_dict[key] = data["Input"][
                    data.Target == key].dropna().tolist()
            density_plot = {
                "data": data_frame_dict
            }
            attr.metrics["density_plot"] = density_plot
        elif plot_type == PlotType.MOSAIC.value:
            mosaic_plot = {
                "data": data.to_dict(orient="list")
            }
            attr.metrics["mosaic_plot"] = mosaic_plot
